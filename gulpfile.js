var gulp      = require('gulp'),
    sass      = require('gulp-ruby-sass'),
    prefix    = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    lr = require('tiny-lr'),
    livereload = require('gulp-livereload'),
    server = lr();

gulp.task('compileStyles', function() {
    gulp.src('program/sass/main.{sass,scss}')
    .pipe(
        sass('program/sass/main.{sass,scss}')
		.on('error', sass.logError)
        .pipe(prefix('last 3 version'))
        .pipe(minifycss())
        .pipe(gulp.dest('dist/css'))
        .pipe(livereload(server))
    );
});



gulp.task('watch', function() {
    server.listen(35729, function( err ) {
        if ( err ) { return console.log( err ); }
        gulp.watch('program/sass/**/*{sass,scss}', ['compileStyles']);
    });
});

gulp.task('default', ['watch']);
